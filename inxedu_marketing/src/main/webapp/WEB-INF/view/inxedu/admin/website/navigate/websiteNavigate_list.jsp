<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>导航管理</title>
<script type="text/javascript">
	function delNavigate(id){
		if(confirm("真的要删除吗？")==true){
			$.ajax({
				url:"${cxt}/admin/website/delNavigate/"+id,
				type:"post",
				dataType:"json",
				success:function(result){
					if(result.message=="true"){
						msgshow("删除成功");
						window.location.reload();
					}
				}
			});
		}
	} 
	//冻结或解冻导航
	function freezeNavigate(id,status){
		$.ajax({
			url:"${cxt}/admin/website/freezeNavigate",
			type:"post",
			data:{"websiteNavigate.id":id,"websiteNavigate.status":status},
			dataType:"json",
			success:function(result){
				if(result.message=="true"){
					if(status==0){
						msgshow("解冻成功",'true');
					}else{
						msgshow("冻结成功",'true');
					}
					window.location.reload();
				}
			}
		});
	}
	/*新建导航的冻结解冻*/
	function lockNav(obj) {
		var lock = $(obj).html();
		var navigateStatus = $(obj).parent().parent().find(".navigateStatus input").val();
		if (lock=="冻结"&&navigateStatus=="0"){
			$(obj).html("解冻");
			$(obj).parent().parent().find(".navigateStatus").html("<input type='hidden' value='1'>冻结")
		}
		if (lock=="解冻" &&navigateStatus=="1"){
			$(obj).html("冻结");
			$(obj).parent().parent().find(".navigateStatus input").val(0);
			$(obj).parent().parent().find(".navigateStatus").html("<input type='hidden' value='0'>正常")

		}
	}
	/*保存导航*/
	function saveNav(obj,navId) {
		var name = $(obj).parent().parent().find(".navigateName input").val();
		var url = $(obj).parent().parent().find(".navigateUrl input").val();
		var oldUrl = $(obj).parent().parent().find(".navigateUrl").attr("title")
		var newPage = $(obj).parent().parent().find(".navigateNewPage select").val();
		var orderNum = $(obj).parent().parent().find(".navigateOrderNum input").val();
		var status = $(obj).parent().parent().find(".navigateStatus input").val();
		if(name==null||name==""){
			msgshow("名称不能为空!",'false');
			return false;
		}
		if(url==null||url==""){
			msgshow("跳转链接不能为空!",'false');
			return false;
		}
		/*if(url.charAt(0)!="/"){
			msgshow("请按正确格式填写链接!",'false');
			return false;
		}*/
		if ("INDEX"=='${type}'){
			if (!isEmpty(url)&&url.substring(url.lastIndexOf("."))!=".html"){
				msgshow("链接必须以.html结尾!",'false');
				return;
			}
		}
		/*如果原url为index 并且url被修改了*/
		if("/index.html"==oldUrl && oldUrl!=url){
			msgshow("不能修改首页链接",'false','500')
			return;
		}
		/*如果不是首页的导航链接被改成index*/
		if("/index.html"==url && oldUrl!="/index.html"){
			msgshow("非首页链接不能为/index.html",'false','500')
			return;
		}
		/*如果是新建的导航*/
		if (navId==null||navId==""){
			$.ajax({
				type:"POST",
				dataType:"json",
				url:"/admin/website/addNavigate",
				data:{"websiteNavigate.name":name,"websiteNavigate.type":"${type}","websiteNavigate.url":url,"websiteNavigate.newPage":newPage,"websiteNavigate.orderNum":orderNum,"websiteNavigate.status":status},
				success:function(result){
					if (result.success){
						$(obj).parent().parent().find(".navigateName").replaceWith("<td class='navigateName'>"+name+"</td>");
						$(obj).parent().parent().find(".navigateUrl").replaceWith("<td class='navigateUrl'>"+url+"</td>");
						if (newPage=="1"){
							$(obj).parent().parent().find(".navigateNewPage").replaceWith("<td class='navigateNewPage'>否</td>");
						}else {
							$(obj).parent().parent().find(".navigateNewPage").replaceWith("<td class='navigateNewPage'>是</td>");
						}
						$(obj).parent().parent().find(".navigateOrderNum").replaceWith("<td class='navigateOrderNum'><input type='hidden' value='"+result.entity.orderNum+"'>" +
								"<a href='javascript:void(0)' onclick='moveNavUp(this,"+result.entity.id+")' title='向上一级' class='acd-ico-btn a-i-b-up'></a>"+
						"<a href='javascript:void(0)'onclick='moveNavDown(this)' title='向下一级' class='acd-ico-btn a-i-b-dwon'></a></td>");
						$(obj).parent().parent().find(".save").attr("onclick","update("+result.entity.id+",this)");
						$(obj).parent().parent().find(".save").html("修改");
						$(obj).parent().parent().find(".delete").attr("onclick","delNavigate("+result.entity.id+")");
						if (result.entity.status=="0"){
							$(obj).parent().parent().find(".lock").attr("onclick","freezeNavigate("+result.entity.id+",1)");
						}else {
							$(obj).parent().parent().find(".lock").attr("onclick","freezeNavigate("+result.entity.id+",0)");
						}

					}else {
						msgshow(result.message)
					}
				}
			});
		}else {
			/*如果不是新建的导航*/
			var name = $(obj).parent().parent().find(".navigateName input").val();
			var url = $(obj).parent().parent().find(".navigateUrl input").val();
			var newPage = $(obj).parent().parent().find(".navigateNewPage select").val();
			var orderNum = $(obj).parent().parent().find(".navigateOrderNum input").val();
			var status = $(obj).parent().parent().find(".navigateStatus input").val();
			var type = "${type}";
			$.ajax({
				type:"POST",
				dataType:"json",
				url:"/admin/website/updateNavigate",
				data:{"websiteNavigate.id":navId,"websiteNavigate.name":name,"websiteNavigate.url":url,"websiteNavigate.newPage":newPage,"websiteNavigate.type":type,"websiteNavigate.orderNum":orderNum,"websiteNavigate.status":status},
				success:function(result){
					if (result.success){
						$(obj).parent().parent().find(".navigateName").replaceWith("<td class='navigateName'>"+name+"</td>");
						$(obj).parent().parent().find(".navigateUrl").replaceWith("<td  class='navigateUrl'>"+url+"</td>");
						if (newPage=="1"){
							$(obj).parent().parent().find(".navigateNewPage").replaceWith("<td class='navigateNewPage'>否</td>");
						}else {
							$(obj).parent().parent().find(".navigateNewPage").replaceWith("<td class='navigateNewPage'>是</td>");
						}
						$(obj).parent().parent().find(".navigateOrderNum").replaceWith("<td class='navigateOrderNum'><input type='hidden' value='"+orderNum+"'><a href='javascript:void(0)' onclick='moveNavUp(this,"+result.entity.id+")' title='向上一级' class='acd-ico-btn a-i-b-up'></a>"+
						"<a href='javascript:void(0)'onclick='moveNavDown(this)' title='向下一级' class='acd-ico-btn a-i-b-dwon'></a></td>");
						$(obj).parent().parent().find(".save").attr("onclick","update("+result.entity.id+",this)");
						$(obj).parent().parent().find(".save").html("修改");
						$(obj).parent().parent().find(".delete").attr("onclick","delNavigate("+result.entity.id+")");
						if (result.entity.status=="0"){
							$(obj).parent().parent().find(".lock").attr("onclick","freezeNavigate("+result.entity.id+",1)");
						}else {
							$(obj).parent().parent().find(".lock").attr("onclick","freezeNavigate("+result.entity.id+",0)");
						}
					}else {
						msgshow(result.message,"false","500")
					}
				}
			});
		}

	}
	/*点击修改后将标签换成input修改内容*/
	function update(navId,obj) {
		var name = $(obj).parent().parent().find(".navigateName").html();
		var url = $(obj).parent().parent().find(".navigateUrl").html();
		var newPage = $(obj).parent().parent().find(".navigateNewPage input").val();
		var orderNum = $(obj).parent().parent().find(".navigateOrderNum input").val();
		var status = $(obj).parent().parent().find(".navigateStatus input").val();
		$(obj).parent().parent().find(".navigateName").replaceWith("<td class='navigateName'><input value="+name+"></td>");

		$(obj).parent().parent().find(".navigateUrl").replaceWith("<td title='"+url+"' class='navigateUrl'><input  value="+url+"></td>");
		if (newPage=="1"){
			$(obj).parent().parent().find(".navigateNewPage").replaceWith("<td class='navigateNewPage'><select ><option value ='1'>否</option><option value ='0'>是</option></select></td>");
		}else {
			$(obj).parent().parent().find(".navigateNewPage").replaceWith("<td class='navigateNewPage'><select ><option value ='0'>是</option><option value ='1'>否</option></select></td>");
		}
		$(obj).parent().parent().find(".navigateOrderNum").replaceWith("<td class='navigateOrderNum'><input type='hidden' value="+orderNum+">" +
				"<a href='javascript:void(0)' onclick='moveNavUp(this,"+navId+")' title='向上一级' class='acd-ico-btn a-i-b-up'></a>"+
		"<a href='javascript:void(0)'onclick='moveNavDown(this,"+navId+")' title='向下一级' class='acd-ico-btn a-i-b-dwon'></a></td>");
		$(obj).parent().parent().find(".save").attr("onclick","saveNav(this,"+navId+")");
		$(obj).parent().parent().find(".save").html("保存");
		$(obj).parent().parent().find(".lock").attr("onclick","lockNav(this)");
	}
	/*上移操作*/
	function moveNavUp(obj,navId) {
		var orderNum = $(obj).parent().find("input").val();
		$.ajax({
			type:"POST",
			dataType:"json",
			url:"/admin/website/moveNavUp",
			data:{"navId":navId,"orderNum":orderNum},
			success:function(result){
				$(obj).parent().find("input").val(result.message);
				$(obj).parent().parent().prev().find(".navigateOrderNum input").val(orderNum);
				$(obj).parent().parent().insertBefore($(obj).parent().parent().prev());
			}
		});
	}
	/*下移操作*/
	function moveNavDown(obj,navId) {
		var orderNum = $(obj).parent().find("input").val();
		$.ajax({
			type:"POST",
			dataType:"json",
			url:"/admin/website/moveNavDown",
			data:{"navId":navId,"orderNum":orderNum},
			success:function(result){
				$(obj).parent().find("input").val(result.message);
				$(obj).parent().parent().next().find(".navigateOrderNum input").val(orderNum);
				$(obj).parent().parent().next().insertBefore($(obj).parent().parent());
			}
		});
	}
</script>
</head>
<body>
	<!-- /tab1 begin-->
	<div class="rMain rMain-nb">
		<div class="commonWrap">
			<form action="${ctx}/admin/website/navigates" name="searchForm" id="searchForm" method="post">
				<table cellspacing="0"  cellpadding="0" border="0" width="100%" class="fullwidth">
					<%--<caption>
						<div class="mt10 clearfix">
							<p class="fl c_666">
								导航类型：
								<select name="websiteNavigate.type" id="type" class="sele-box">
									<option value="">请选择</option>
									<option value="INDEX" <c:if test="${websiteNavigate.type=='INDEX'}">selected="selected"</c:if>>首页</option>
									<option value="FRIENDLINK" <c:if test="${websiteNavigate.type=='FRINEDLINK'}">selected="selected"</c:if>>尾部友链</option>
									<option value="TAB" <c:if test="${websiteNavigate.type=='TAB'}">selected="selected"</c:if>>尾部标签</option>
								</select>
								<input type="submit" class="button" value="查询" />
								<input type="button" class="button" value="清空" onclick="$('#type').val('');" />
								<input type="button" class="button" value="添加" onclick="window.location.href='${cxt}/admin/website/doAddNavigates'" />
							</p>
						</div>
					</caption>--%>
					<thead>
						<tr>
							<td>
								<span>名称</span>
							</td>
							<td>
								<span>跳转地址</span>
							</td>
							<td>
								<span>在新页面打开</span>
							</td>
							<td>
								<span>排序</span>
							</td>
							<td>
								<span>状态</span>
							</td>
							<td>
								<span>操作</span>
							</td>
						</tr>
					</thead>
					<tbody id="tabS_02" align="center">
						<c:if test="${websiteNavigates.size()>0}">
							<c:forEach items="${websiteNavigates}" var="navigate" varStatus="index">
								<tr <c:if test="${index.count%2==1 }">class="odd"</c:if>>
									<td class="navigateName">${navigate.name }</td>
									<td class="navigateUrl">${navigate.url }</td>
									<td class="navigateNewPage">
										<c:if test="${navigate.newPage==0 }"><input type="hidden" value="0">是</c:if>
										<c:if test="${navigate.newPage==1 }"><input type="hidden" value="1">否</c:if>
									</td>
									<td class="navigateOrderNum">
										<input type="hidden" value="${navigate.orderNum}"/>
										<a href="javascript:void(0)" onclick="moveNavUp(this,${navigate.id})" title="向上一级" class="acd-ico-btn a-i-b-up"></a>
										<a href="javascript:void(0)"onclick="moveNavDown(this,${navigate.id})" title="向下一级" class="acd-ico-btn a-i-b-dwon"></a>
									</td>
									<td class="navigateStatus">
										<c:if test="${navigate.status==0 }"><input type="hidden"value="0">正常</c:if>
										<c:if test="${navigate.status==1 }"><input type="hidden"value="1">冻结</c:if>
									</td>
									<td class="c_666 czBtn" align="center">
										<button type="button" class="ui-state-default ui-corner-all ui-btn save"
											onclick="update(${navigate.id},this)">修改</button>
										<c:if test="${navigate.pageId>49||navigate.pageId==0}">
										<button type="button" class="ui-state-default ui-corner-all ui-btn delete" onclick="delNavigate(${navigate.id})">删除</button>
										</c:if>
										<c:if test="${navigate.status==0}">
											<button type="button" class="ui-state-default ui-corner-all ui-btn lock" onclick="freezeNavigate(${navigate.id},1)">冻结</button>
										</c:if>
										<c:if test="${navigate.status==1}">
											<button type="button" class="ui-state-default ui-corner-all ui-btn lock" onclick="freezeNavigate(${navigate.id},0)">解冻</button>
										</c:if>
									</td>
								</tr>
							</c:forEach>
						</c:if>
						<c:if test="${websiteNavigates.size()==0||websiteNavigates==null}">
							<tr>
								<td align="center" colspan="16">
									<div class="tips">
										<c:if test="${type=='index'}">
										<span>还没有导航！</span>
										</c:if>
										<c:if test="${type=='link'}">
											<span>还没有链接！</span>
										</c:if>
									</div>
								</td>
							</tr>
						</c:if>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</body>
</html>