<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/base.jsp"%>
${header}
<div id="temTwo">
    <section id="demo4-main" class="all-main">
        <div class="new-list-warp">
            <section class="container">
                <div class="comm-title">
                    <header class="clearfix">
                        <h3 class="unFw hLh30 pb10 fl">
                            <span class="disIb fsize24 f-fH c-333">资讯详情</span>
                            <span class="disIb fsize24 f-fM c-master ml5 mr5">/</span>
                            <span class="disIb fsize12 f-fM c-999">DETAILS</span>
                        </h3>
                        <span class="fr hLh20 mt10">
							<a class="go-back disFw" style="text-decoration: none" href="javascript:history.go(-1)" title="返回">
								<em class="icon20 ico">
									<img src="${ctx}/template/templet1/images/go-back.png">
								</em>
								<tt class="fsize14 c-999 f-fM">返回</tt>
							</a>
						</span>
                    </header>
                </div>
            </section>
            <div>
                <section class="container">
                    <article class="mt50 pb20 n-a-tit">
                        <h2 class="tac unFw">
                            <font class="fsize24 c-333 f-fH">${r'${article.title}'}</font>
                        </h2>
                        <div class="hLh30 tac mt10 of fenl">
							<span class="disIb">
								<em class="icon16 ico">
									<img src="${ctx}/template/templet1/images/liul.png">
								</em>
								<tt class="fsize12 c-999 f-fM vam">${r'${article.clickNum}'}次</tt>
							</span>
                            <span class="disIb mr30 ml30">
								<em class="icon16 ico">
									<img src="${ctx}/template/templet1/images/time.png">
								</em>
								<tt class="fsize12 c-999 f-fM vam"> ${r'${date}'}</tt>
							</span>
                            <span class="disIb">
								<em class="icon16 ico">
									<img src="${ctx}/template/templet1/images/laiy.png">
								</em>
								<tt class="fsize12 c-999 f-fM vam">${r'${article.author}'}</tt>
							</span>
                        </div>
                    </article>
                    <article class="n-a-warp mt20">
                        <p>${r'${content}'}</p>
                    </article>
                </section>
            </div>
        </div>
    </section>
</div>
${footer}

