package com.inxedu.os.common.constants;

import com.inxedu.os.common.util.PropertyUtil;

/**
 * @description cache缓存相关常量
 * @author www.inxedu.com
 */
public class CacheConstans {
	public static PropertyUtil webPropertyUtil = PropertyUtil.getInstance("memtimes");
	public static final String MEMFIX = webPropertyUtil.getProperty("memfix");
	public static final String RECOMMEND_COURSE = MEMFIX + "recommend_course";
	public static final int RECOMMEND_COURSE_TIME = Integer.parseInt(webPropertyUtil.getProperty("RECOMMEND_COURSE_TIME"));
	public static final String BANNER_IMAGES = MEMFIX + "banner_images";
	public static final int BANNER_IMAGES_TIME = Integer.parseInt(webPropertyUtil.getProperty("BANNER_IMAGES_TIME"));
	public static final String WEBSITE_PROFILE = MEMFIX + "website_profile";
	public static final int WEBSITE_PROFILE_TIME = Integer.parseInt(webPropertyUtil.getProperty("WEBSITE_PROFILE_TIME"));
	public static final String WEBSITE_NAVIGATE = MEMFIX + "website_navigate";
	public static final int WEBSITE_NAVIGATE_TIME = Integer.parseInt(webPropertyUtil.getProperty("WEBSITE_NAVIGATE_TIME"));

	/** 缓存后台登录用户memCache前缀 */
	public static final String LOGIN_MEMCACHE_PREFIX ="login_sys_user";
	/** 后台所有用户权限缓存名前缀 **/
	public static final String SYS_ALL_USER_FUNCTION_PREFIX = "SYS_USER_ALL_FUNCTION_";
	/** 登录用户权限缓存名前缀 **/
	public static final String USER_FUNCTION_PREFIX = "SYS_USER_FUNCTION_";
	
	/** 文章  好文推荐 缓存 **/
	public static final String 	ARTICLE_GOOD_RECOMMEND = MEMFIX+"ARTICLE_GOOD_RECOMMEND";


	/* 动态模板数据*/
	public static final String TEMPLATE_DATA = MEMFIX + "template_data";
	/*动态模板数据 5分钟*/
	public static final int TEMPLATE_DATA_TIME = Integer.parseInt(webPropertyUtil.getProperty("TEMPLATE_DATA_TIME"));
}
