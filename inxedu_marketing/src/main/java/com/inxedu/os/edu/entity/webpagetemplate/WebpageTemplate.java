package com.inxedu.os.edu.entity.webpagetemplate;

import lombok.Data;

import java.io.Serializable;

/**
 * @author www.inxedu.com
 * @description 页面和模板中间表
 */
@Data
public class WebpageTemplate implements Serializable {
    private static final long serialVersionUID = 3148176768559230877L;

	/** id */
	private Long id;
	/** 页面id */
	private Long pageId;
	/** 模板名称 */
	private String templateName;
	/** 模板标题（页面显示） */
	private String templateTitle;
	/** 引用的模板路径 */
	private String templateUrl;
	/** 信息、描述、备注等 */
	private String info;
	/** 排序值 */
	private int sort;
	/** 是否生效 1生效（默认） 2预览（新增的为2，预览查看所有，发布只查询1的，正式发布后修改为1）*/
	private int effective;

}

