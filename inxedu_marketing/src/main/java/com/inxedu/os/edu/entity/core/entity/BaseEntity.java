package com.inxedu.os.edu.entity.core.entity;

import java.io.Serializable;

/**
 * @author www.inxedu.com
 */
public class BaseEntity implements Serializable {
	private static final long serialVersionUID = 1078459635026071151L;

	public BaseEntity() {
	}
}