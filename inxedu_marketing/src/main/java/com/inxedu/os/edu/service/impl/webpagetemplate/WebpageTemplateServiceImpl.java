package com.inxedu.os.edu.service.impl.webpagetemplate;

import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.dao.webpagetemplate.WebpageTemplateDao;
import com.inxedu.os.edu.entity.webpagetemplate.WebpageTemplate;
import com.inxedu.os.edu.service.webpagetemplate.WebpageTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author www.inxedu.com
 * @description 页面和模板中间表 WebpageTemplateService接口实现
 */
@Service("webpageTemplateService")
public class WebpageTemplateServiceImpl implements WebpageTemplateService {
	@Autowired
	private WebpageTemplateDao webpageTemplateDao;
	
	/**
     * 添加页面和模板中间表
     */
    public Long addWebpageTemplate(WebpageTemplate webpageTemplate){
		return webpageTemplateDao.addWebpageTemplate(webpageTemplate);
    }
    
    /**
     * 删除页面和模板中间表
     * @param id
     */
    public void delWebpageTemplateById(Long id){
    	webpageTemplateDao.delWebpageTemplateById(id);
    }
    
    /**
     * 修改页面和模板中间表
     * @param webpageTemplate
     */
    public void updateWebpageTemplate(WebpageTemplate webpageTemplate){
    	webpageTemplateDao.updateWebpageTemplate(webpageTemplate);
    }
    
    /**
     * 通过id，查询页面和模板中间表
     * @param id
     * @return
     */
    public WebpageTemplate getWebpageTemplateById(Long id){
    	return webpageTemplateDao.getWebpageTemplateById(id);
    }
    
    /**
     * 分页查询页面和模板中间表列表
     * @param webpageTemplate 查询条件
     * @param page 分页条件
     * @return List<WebpageTemplate>
     */
    public List<WebpageTemplate> queryWebpageTemplateListPage(WebpageTemplate webpageTemplate,PageEntity page){
    	return webpageTemplateDao.queryWebpageTemplateListPage(webpageTemplate, page);
    }
    
    /**
     * 条件查询页面和模板中间表列表
     * @param webpageTemplate 查询条件
     * @return List<WebpageTemplate>
     */
    public List<WebpageTemplate> queryWebpageTemplateList(WebpageTemplate webpageTemplate){
    	return webpageTemplateDao.queryWebpageTemplateList(webpageTemplate);
    }

    @Override
    public void delWebpageTemplateByWebpageId(Long webpageId) {
        webpageTemplateDao.delWebpageTemplateByWebpageId(webpageId);
    }

    @Override
    public void updWebpageTemplatesSort(int oneSort, Long oneId, int twotSort,Long twoId) {
        WebpageTemplate webpageTemplate = this.getWebpageTemplateById(oneId);
        webpageTemplate.setSort(twotSort);
        this.updateWebpageTemplate(webpageTemplate);

        WebpageTemplate twowebpageTemplate = this.getWebpageTemplateById(twoId);
        twowebpageTemplate.setSort(oneSort);
        this.updateWebpageTemplate(twowebpageTemplate);
    }

    @Override
    public void delWebpageTemplate(WebpageTemplate webpageTemplate) {
        webpageTemplateDao.delWebpageTemplate(webpageTemplate);
    }
}



