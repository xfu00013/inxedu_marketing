package com.inxedu.os.edu.service.websiteimagesdefault;

import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.entity.websiteimagesdefault.WebsiteImagesDefault;

import java.util.List;

/**
 * @author www.inxedu.com
 * @description 模板默认数据（还原用，后期根据方案优化） WebsiteImagesDefaultService接口
 */
public interface WebsiteImagesDefaultService{
	/**
     * 添加模板默认数据（还原用，后期根据方案优化）
     */
    Long addWebsiteImagesDefault(WebsiteImagesDefault websiteImagesDefault);
    
    /**
     * 删除模板默认数据（还原用，后期根据方案优化）
     * @param imageId
     */
    void delWebsiteImagesDefaultByImageId(Long imageId);
    
    /**
     * 修改模板默认数据（还原用，后期根据方案优化）
     * @param websiteImagesDefault
     */
    void updateWebsiteImagesDefault(WebsiteImagesDefault websiteImagesDefault);
    
    /**
     * 通过imageId，查询模板默认数据（还原用，后期根据方案优化）
     * @param imageId
     * @return
     */
    WebsiteImagesDefault getWebsiteImagesDefaultByImageId(Long imageId);
    
    /**
     * 分页查询模板默认数据（还原用，后期根据方案优化）列表
     * @param websiteImagesDefault 查询条件
     * @param page 分页条件
     * @return List<WebsiteImagesDefault>
     */
    List<WebsiteImagesDefault> queryWebsiteImagesDefaultListPage(WebsiteImagesDefault websiteImagesDefault, PageEntity page);
    
    /**
     * 条件查询模板默认数据（还原用，后期根据方案优化）列表
     * @param websiteImagesDefault 查询条件
     * @return List<WebsiteImagesDefault>
     */
    List<WebsiteImagesDefault> queryWebsiteImagesDefaultList(WebsiteImagesDefault websiteImagesDefault);
}



